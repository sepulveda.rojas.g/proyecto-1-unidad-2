_**Proyecto N°1 - Unidad 2**_

**Alcance**

Se diseña un software de gestión de notas para la asignatura de programación I en la carrera de ingeniería civil en bioinformática, con la finalidad de ordenar 

**Actores:**

- Profesor a cargo
- Estudiantes

**Acciones a realizar**

- Menú de inicio
- Añadir estudiantes
- Eliminar estudiantes
- Modificar nota del estudiante
- Enlistar por orden alfabético
- Enlista por nota decreciente
- Saber el promedio general del curso

