# Nombre: Gabriela Antonia Sepúlveda Rojas
# RUT: 20.787.581-3
# Fecha de entrega: 17 de diciembre

# Programación I es uno de los módulos del segundo semestre en Ingeniería Civil en Bioinformática de la Universidad de Talca, la cual cursan 
# miles de estudiantes para familiarizarse en la informática a nivel general. Una de sus últimas evaluaciones consta de un proyecto, el cual
# definirá el resultado de su año académico, y para esto es necesario crear un sistema que nos ayude a crear en una lista los datos de cada estudiante y
# saber el promedio general del curso. La finalidad de esto es saber cómo está el rendimiento de los estudiantes de esta carrera, además de poder
# mostrar de forma ordenada los resultados.

# Entrada: Para comenzar crearemos un menú para que la persona que utilice el sistema vea las opciones a las cuales puede acceder. La finalidad 
# es que pueda navegar y modificar el sistema para la finalidad principal, que es ingresar los estudiantes de ingeniería civil en bioinformática
# y su evaluacion en la creación del proyecto N°2.

#Procedimiento:

# Salida: Dependiendo el código que haya utilizado el usuario se imprimirá en la pantalla una lista de los alumnos y calificaciones ingresadas,
# también la posibilidad eliminar a un estudiante e incluso modificar la nota que se ha ingresado anteriormente, todo esto perfectamente alineado
# para una clara representación.

def Entrarnombre():

    while True:
        nombre = input("Ingrese el nombre del estudiante: ")

        if nombre == "":
            print("Ingrese un nombre, no puede estar vacío.")

        else:
            return nombre

def Entrarnota():

    while True:
        try:
            nota = float(input("Ingrese la nota del estudiante: "))

            if 1 <= nota <= 7:
                return nota

            else:
                print("La nota del estudiante debe estar entre 1.0 y 7.0")

        except:
            print("La nota del estudiante debe ser un valor numérico, intente nuevamente.")

def DevolverEstudiante():
    nombre = Entrarnombre()

    for el in Estudiantes:
        if el[0] == nombre:
            print("La nota del estudiante '{}' es {}".format(nombre, el[1]))
            return True
    print("No se encuentra el estudiante en el sistema.")
    return False

def ModificarNota():
    nombre = Entrarnombre()
    indice = BuscarEstudiante(nombre)

    if indice == -1:
        print("No se ha encontrado el estudiante '{}'",format(nombre))
        return False

    tmp = list(Estudiantes[indice])
    tmp[1] = Entrarnota()
    Estudiantes[indice] = tuple(tmp)
    print("Se ha actualizado la nota del estudiante '{}'".format(nombre))
    return True

def ListarEstudiantesNombre():
    Estudiantes.sort()

    print("\n".join(i[0]+" - "+str(i[1]) for i in Estudiantes))

def ListarestudiantesNota():
    Estudiantes.sort(key = lambda x: x[1], reverse = True)

    print("\n".join(i[0]+" - "+str(i[1]) for i in Estudiantes))

def MediaNotas():

    if len (Estudiantes) == 0:
        print("No se encuentran estudiantes ingresados en el sistema.")
        return
    
    Media = sum([i[1] for i in Estudiantes])/len(Estudiantes)
    print ("El promedio general del curso es de '{}'".format(Media))

def BorrarEstudiante():
    nombre = Entrarnombre()
    indice = BuscarEstudiante(nombre)

    if indice == -1:
        print("No se ha encontrado el estudiante '{}'".format(nombre))
        return False
    
    print("Se ha eliminado el estudiante '{}' con nota {}".format(nombre, Estudiantes[indice][1]))
    del Estudiantes[indice]
    return True

def BuscarEstudiante(nombre):

    for i, e in enumerate(Estudiantes):
        if e[0] == nombre:
            return i
    return -1

def Menú():
    print("\n - B i e n v e n i d o - a l - s i s t e m a - d e - n o t a s - d e l - p r o y e c t o - N ° 2")
    print ("\n\t1 - Añadir estudiante")
    print ("\t2 - Buscar estudiante")
    print ("\t3 - Modificar nota del estudiante")
    print ("\t4 - Borrar un estudiante")
    print ("\t5 - Listado de los estudiantes ordenado alfabéticamente por el nombre")
    print ("\t6 - Listado de las notas de los estudiantes ordenado de forma descendente")
    print ("\t7 - Promedio general del curso")
    print ("\t0 - Salir")

Estudiantes = []

while True:
    Menú()

    try:
        opcion = int(input("\nIngrese la opción que desea escoger: "))
    except:
        opcion = -1
    
    if opcion == 1:
        Estudiantes.append((Entrarnombre(), Entrarnota()))

    elif opcion == 2:
        DevolverEstudiante()

    elif opcion == 3:
        ModificarNota()

    elif opcion == 4:
        BorrarEstudiante()
    
    elif opcion == 5:
        ListarEstudiantesNombre()
    
    elif opcion == 6:
        ListarestudiantesNota()

    elif opcion == 7:
        MediaNotas()
    
    elif opcion == 0:
        break

    else:
        print ("La opción ingresada no es correcta, intente nuevamente.")